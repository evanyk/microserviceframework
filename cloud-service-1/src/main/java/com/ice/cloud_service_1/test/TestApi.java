package com.ice.cloud_service_1.test;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/ribbon")
public class TestApi {
	
	@RequestMapping(value="name", method = RequestMethod.GET)
	public String getStr() {
		return "Message from cloud-service-1";
	}
}
