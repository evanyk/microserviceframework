package com.ice.cloud_ribbon_client.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
@RequestMapping(value="/test")
public class TestRibbon {
	
	@Autowired
	RestTemplate restTemplate;
	
	
	@RequestMapping(value="/api", method=RequestMethod.GET)
	@HystrixCommand(fallbackMethod="serviceFailure")
	public String testApi() {
		String url = "http://CLOUD-SERVICE/ribbon/name";
		return restTemplate.getForObject(url, String.class);
	}
	
	public String serviceFailure() {
		return "Cloud service is not available!";
	}
}
