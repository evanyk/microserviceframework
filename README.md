# MicroServiceFramework

<1> Config Servers 配置服务器
  eureka-center - eureka-config
                       |
                  eureka-config-client

<2> Ribbon Servers 均衡负载&熔断机制
  eureka-center - cloud-ribbon-client
                       |
                       - cloud-service-1
                       - cloud-service-2

<3> Route Servers 路由转发&均衡负载&熔断
  eureka-center - cloud-zuul-client
                    |
                    - cloud-ribbon-client
                        |
                        - cloud-service-1
                        - cloud-service-2

<4> Dashboard 路由转发&均衡负载&熔断&负载监控
  eureka-center - cloud-zuul-client
       |             |
  turbine-dashboard  - cloud-ribbon-client
                        |
                        - cloud-service-1
                        - cloud-service-2                                               

