package com.ice.eureka_config_client.test;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
public class TestApi {
	
	@Value("${myblog.name}")
	private String name;
	
	@Value("${myblog.url:default url}")
	private String url;
	
	@Value("${myblog.location:default location}")
	private String location;
	
	@RequestMapping("/info")
	public String getInfo() {
		return "get info: [" +name+"," +url+"," +location + "]";
	}
}
