package com.ice.cloud_zuul_client.filter;

import javax.servlet.http.HttpServletRequest;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

public class TestFilter extends ZuulFilter{

	@Override
	public boolean shouldFilter() {
		// If using filter
		return false;
	}

	@Override
	public Object run() throws ZuulException {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		String username = request.getParameter("token");
        if (null != username && username.equals("evan")) {
        	// http://localhost:8097/ribbon/test/api?token=evan
            ctx.setSendZuulResponse(true);
            ctx.setResponseStatusCode(200);
            ctx.set("isSuccess", true);
            return null;
        } else {
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(403);
            ctx.setResponseBody("{\"result\":\"Request illegal!the token is null\"}");
            ctx.set("isSuccess", false);
            return null;
        }
	}

	@Override
	public String filterType() {
		// [pre; routing; post; error]
		return "pre";
	}

	@Override
	public int filterOrder() {
		// [pre; routing; post; error]
		return 0;
	}

}
