package com.ice.cloud_zuul_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

import com.ice.cloud_zuul_client.filter.TestFilter;

@SpringBootApplication
@EnableEurekaClient
@EnableZuulProxy
public class App 
{
    public static void main( String[] args )
    {
        SpringApplication.run(App.class, args);
    }
    
    @Bean
    public TestFilter accessFilter() {
    	return new TestFilter();
    }
}
